﻿using UnityEngine;

public class GrowthbeatSampleComponent : MonoBehaviour
{
	string applicationId = "RRYCNydiBqVqWeHg";
	string credentialId = "5ZX6DjYnruHHRIo8ozTzaGzaRJo3WSgz";
	string senderId = "229333439105";
	void Awake()
	{
		GrowthPush.GetInstance().Initialize(applicationId, credentialId,
			Debug.isDebugBuild ? GrowthPush.Environment.Development : GrowthPush.Environment.Production);

		GrowthPush.GetInstance().RequestDeviceToken(senderId);

		string devicetoken = GrowthPush.GetInstance().GetDeviceToken();
		Debug.Log(devicetoken);


		GrowthPush.GetInstance().SetTag("development", "true");

		GrowthPush.GetInstance().TrackEvent("Launch");
	}
	void Start()
	{

	}

	void Update()
	{

	}
}
